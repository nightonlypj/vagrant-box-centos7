# CentOS7 Vagrant Box提供(VirtualBox向け)

## 提供バージョン

- 7.3.1611 [CentOS-7-x86_64-Minimal-1611.iso]

## 前提条件

下記がインストールされている事

- Vagrant ( https://www.vagrantup.com/downloads.html )  
- VirtualBox ( https://www.virtualbox.org/wiki/Downloads )

※古いバージョンでは動かない場合があります。

作成時に使用したバージョン

- Vagrant 1.9.3 (Windows 64-bit)  
- VirtualBox 5.1.22 (Windows)

## 使用方法(例)

Windowsコマンドプロンプト/Mac・Linuxターミナル  
$ `vagrant box add CentOS7 https://gitlab.com/nightonlypj/vagrant-box-centos7/raw/master/CentOS7.3.1611.box`  
$ `vagrant init CentOS7`  
$ `vagrant up`

※[CentOS7 Vagrantfile＋Ansible playbook提供](https://gitlab.com/nightonlypj/vagrant-ansible-centos7)でも使用しています。
